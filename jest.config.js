// jest.config.js
module.exports = {
  roots: ["<rootDir>/src"],
  setupFiles: ["<rootDir>/node_modules/regenerator-runtime/runtime"],
  // reporters: [
  //   ['jest-junit', {
  //     outputDirectory: '<rootDir>/reports/tests',
  //     outputName: 'test-junit.xml',
  //     suiteName: 'mp-transport',
  //     suiteNameTemplate: '{filepath}',
  //     classNameTemplate: '{classname}',
  //     titleTemplate: '{title}',
  //     ancestorSeparator: ' > '
  //   }],
  // ],
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.{js,vue}", "!**/src/main.js"],
  coverageDirectory: "<rootDir>/reports/coverage",
  coverageReporters: ["html"],
  moduleNameMapper: {
    "\\.(css|scss)": "identity-obj-proxy",
    "\\.(svg|png|jpg|jpeg)": "<rootDir>/__mocks__/filemock.js",
    "@/utils/workflow": "<rootDir>/src/utils/workflow.json",
    "@ijl.cli": "<rootDir>/__mocks__/cli.js",
  },
  testRegex: "(/__tests__/.*|(\\.|/))+(test|spec)\\.(js)?$",
  snapshotSerializers: ["jest-serializer-vue"],
  moduleFileExtensions: ["js", "json", "vue"],
  transformIgnorePatterns: ["/node_modules/(?!@mplt/ui-kit).+$"],
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.vue$": "vue-jest",
  },
  globals: {
    __webpack_public_path__: "",
  },
};
