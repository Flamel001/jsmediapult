import Landos from "../screens/Landos";
import CapacityPick from "../screens/CapacityPick";
import AdSelection from "../screens/AdSelection";
import Basket from "../screens/Basket";
import RegistrationScreen from "../screens/RegistrationScreen";

export const processes = {
  transportFlow: {
    landing: Landos,
    capacityPick: CapacityPick,
    adSelection: AdSelection,
    registrationScreen: RegistrationScreen,
    basket: Basket,
  },
  elevatorFlow: {
    state1: Landos,
    state2: CapacityPick,
  },
};
