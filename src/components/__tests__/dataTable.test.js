import DataTable from "../DataTable.vue";
import { mount } from "@vue/test-utils";

describe("DataTable", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(DataTable);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
