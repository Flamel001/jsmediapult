import Landos from "../Landos.vue";
import { mount } from "@vue/test-utils";

describe("Landos", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(Landos);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
