import AdSelection from "../AdSelection.vue";
import { mount } from "@vue/test-utils";

describe("AdSelection", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(AdSelection);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
