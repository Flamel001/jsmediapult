import RegistrationScreen from "../RegistrationScreen.vue";
import { mount } from "@vue/test-utils";

describe("RegistrationScreen", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(RegistrationScreen);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
