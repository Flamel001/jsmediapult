import CapacityPick from "../CapacityPick.vue";
import { mount } from "@vue/test-utils";

describe("CapacityPick", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = mount(CapacityPick);
  });

  it("render test", () => {
    expect(wrapper).toMatchSnapshot();
  });
});
