const fs = require("fs");
const path = require("path");
const workflow = require("./workflow");

const router = require("express").Router();

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://89.223.91.151:8080");
  res.header("Access-Control-Allow-Credentials", "true");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

router.get("/workflow", (request, response) => {
  const { cmd, name, data } = request.query;
  let historyList =
    (request.session.workflow && request.session.workflow.historyList) || [];
  console.log(cmd);

  if (historyList.length > 0) {
    historyList[historyList.length - 1].data = data;
  }

  /**
   * @param request
   */
  function reloadSession(request) {
    const { flowName, stateName, data: sendData } = request.session.workflow;

    return { flowName, stateName, sendData };
  }

  function newSession() {
    const flowName = name;
    const stateName = workflow.flows[flowName].init;

    history = {
      id: 0,
      stateName,
      flowName,
      data,
    };

    historyList.push(history);

    return { flowName, stateName, data };
  }

  /**
   * @param request
   */
  function revert(request) {
    let newHistoryList = [];
    let history = {};
    for (let i = 0; i < historyList.length; i++) {
      newHistoryList.push(historyList[i]);
      if (newHistoryList[i].stateName === name) {
        history = newHistoryList[i];
        break;
      }
    }

    historyList = newHistoryList;

    const { flowName, stateName, data: sendData } = history;

    return { flowName, stateName, sendData };
  }

  /**
   * @param flowName
   */
  function newFlow(flowName) {
    return workflow.flows[flowName].init;
  }

  function newState(eventObject) {
    return eventObject.newState || request.session.workflow.stateName;
  }

  /**
   * @param stateName
   * @param flowName
   */
  function updateHistory(stateName, flowName) {
    const history = {
      id: historyList.length,
      stateName,
      flowName,
    };

    historyList.push(history);
  }

  /**
   * @param request
   */
  function getEvent(request) {
    return workflow.flows[request.session.workflow.flowName].states[
      request.session.workflow.stateName
    ].events[name];
  }

  /**
   * @param eventObject
   * @param request
   */
  function getEventState(eventObject, request) {
    let { flowName, data: sendData } = request.session.workflow;

    flowName = eventObject.newFlow || flowName;

    return { flowName, sendData };
  }

  /**
   * @param request
   */
  function start(request) {
    if (request.session.workflow) {
      return reloadSession(request);
    } else {
      return newSession();
    }
  }

  /**
   * @param request
   */
  function event(request) {
    const eventObject = getEvent(request);
    if (name === "back") {
      historyList.pop();
      historyList.pop();
      console.log("popaem normalno");
    }
    const { flowName } = getEventState(eventObject, request);
    let stateName;
    if (request.session.workflow.flowName !== flowName) {
      stateName = newFlow(flowName);
    } else {
      stateName = newState(eventObject);
    }

    updateHistory(stateName, flowName);

    return { flowName, stateName, sendData: data };
  }

  /**
   * @param request
   */
  function reload(request) {
    const { flowName, stateName } = request.session.workflow;

    return { flowName, stateName, sendData: data };
  }

  const commands = {
    reload,
    start,
    event,
    revert,
  };
  const { sendData, stateName, flowName } = commands[cmd](request);

  request.session.workflow = { stateName, flowName, data, historyList };
  response.send({ flowName, stateName, data: sendData, historyList });
});

module.exports = router;
