describe("Go through all pages", () => {
  beforeEach(() => {
    cy.visit("http://localhost:8099/transport/");
  });

  it("Go forward through app", () => {
    cy.contains("Попробуй сейчас").click();
    cy.contains("Далее").click();
    cy.contains("Далее").click();
    cy.contains("Далее").click();
    cy.contains("Отправить заявку").click();
  });

  it("Check backward button", () => {
    cy.contains("Попробуй сейчас").click();
    cy.get(".backward-button").click();
    cy.contains("Попробуй сейчас").click();
    cy.contains("Далее").click();
  });

  it("Check breadcrumbs", () => {
    cy.contains("Попробуй сейчас").click();
    cy.contains("Далее").click();
    cy.contains("landing").click();
    cy.contains("Попробуй сейчас").click();
    cy.contains("Далее").click();
    cy.contains("Далее").click();
    cy.contains("Далее").click();
    cy.contains("adSelection").click();
    cy.contains("capacityPick").click();
    cy.contains("Далее").click();
    cy.contains("Далее").click();
    cy.contains("landing").click();
  });

  it("Check state storing", () => {
    cy.contains("Попробуй сейчас").click();
    cy.contains("Далее").click();
    cy.contains("Такси").click();
    cy.contains("Далее").click();
    cy.contains("capacityPick").click();
    cy.contains("Такси").siblings(".picked_yes");
  });
});
