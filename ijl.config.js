const pkg = require("./package.json");
const webpack = require("./webpack.develop.js");
module.exports = {
  apiPath: "stubs/api",
  webpackConfig: webpack,
  config: {
    "main.api.base.url": "/api",
    "transport.api.base.url": "/api",
  },
  navigations: {
    news: "/news",
    orgs: "/orgs",
    transport: "/transport",
    ya: "https://yandex.ru",
  },
};
