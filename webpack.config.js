const webpack = require("webpack");
const path = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const { TsConfigPathsPlugin } = require("awesome-typescript-loader");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const webpackCopy = require("copy-webpack-plugin");
const outputDirectory = "dist";

module.exports = {
  mode: "production",
  entry: {
    mainApp: ["./src/main.js"]
  },
  output: {
    filename: "[name].js",
    path: path.resolve(__dirname, outputDirectory),
    libraryTarget: "umd",
    globalObject: `(typeof self !== 'undefined' ? self : this)`
  },
  node: {
    fs: "empty"
  },
  plugins: [
    new CleanWebpackPlugin(),
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      "typeof window": JSON.stringify("object")
    })
  ],
  devtool: "source-map",
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "ui-kit/src/components"),
      'images': path.resolve(__dirname, "static/images"),
    },
    modules: ["node_modules", "src"],
    extensions: [
      ".webpack.js",
      ".web.js",
      ".ts",
      ".tsx",
      ".js",
      ".css",
      ".vue"
    ],
    plugins: [new TsConfigPathsPlugin()]
  },

  module: {
    rules: [
      {parser: {system: false}},
      {
        test: /\.tsx?$/,
        loader: "awesome-typescript-loader"
      },
      {
        test: /\.vue?$/,
        loader: "vue-loader"
      },
      {
        test: /\.(jpe?g|gif|png|ttf|eot|wav|mp3)$/,
        loader: "file-loader"
      },
      {
        test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf)$/,
        loader: 'url-loader?limit=100000'
      },
      {
        test: /\.(svg)(\?v=\d+\.\d+\.\d+)?$/,
        // include: path.resolve(__dirname, 'src/assets/inline'),
        use: [
          {
            loader: 'svg-inline-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'icons/',
            },
          },
        ],
      },
      {
        test: /\.s?css$/,
        use: ["vue-style-loader", "css-loader", {
          loader: "sass-loader",
          options: {
            prependData: "@import 'src/globals.scss'"
          }
        }
        ]
      }
    ],
  }
};
